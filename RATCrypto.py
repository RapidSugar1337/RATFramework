from Crypto.Cipher import ChaCha20, Salsa20, AES, Blowfish
from Crypto.Util.Padding import pad, unpad
from Crypto.Util.number import *

def salsa20(data, key, mode='e', nonce=None): # 16 byte key
    
    if len(key) != 16:
        return b'Length of the key must be 16 bytes!'
    if nonce == None or len(nonce) != 8:
        return b"Specify nonce. It must be 8 bytes long."

    cipher = Salsa20.new( key, nonce = nonce)

    if mode=='e':
        result = cipher.encrypt(data)
    else:
        result = cipher.decrypt(data)
    return result
    
def chacha20(data, key, mode='e', nonce=None): # 32 byte key
    if len(key) != 32:
        return b'Length of the key must be 32 bytes!'
    if nonce == None or len(nonce) != 8:
        return b"Specify nonce. It must be 8 bytes long."

    cipher = ChaCha20.new( key = key, nonce = nonce)
    
    if mode=='e':
        result = cipher.encrypt(data)
    else:
        result = cipher.decrypt(data)
    return result

def aes(data, key, mode, aes_mode, iv = None): # 16 byte key
    if aes_mode == AES.MODE_ECB:
        cipher = AES.new(key, AES.MODE_ECB)
        if mode=='e':
            return cipher.encrypt(pad(data, 16)) 
        else:
            return unpad( cipher.decrypt(data), 16)

    elif aes_mode== AES.MODE_CBC:
        if iv == None or len(iv) != 16:
            return b"Specify nonce. It must be 16 bytes long."
            
        cipher = AES.new(key, AES.MODE_CBC, iv = iv )
        if mode=='e':
            result = cipher.encrypt( pad(data, 16) )
            return result
        else:
            return unpad( cipher.decrypt( data ), 16 )

    elif aes_mode == AES.MODE_CTR:
        if mode=='e':
            nonce = os.urandom(8)
            ivalue = os.urandom(8)
            cipher = AES.new(key, AES.MODE_CTR, nonce = nonce, initial_value = ivalue)
            result = nonce + ivalue + cipher.encrypt(data)
            return result
        else:
            nonce = data[:8]
            ivalue = data[8:16]
            data = data[16:]
            cipher = AES.new(key, AES.MODE_CTR, nonce = nonce, initial_value = ivalue)
            return cipher.decrypt(data)
    else:
        return "Mode is not added yet."


def encrypt(data, cipher, key, nonce=None, iv=None, aes_mode=AES.MODE_CTR):
    if cipher.lower().startswith('chacha'):
        return chacha20(data, key, 'e', nonce)
    elif cipher.lower().startswith('salsa'):
        return salsa20(data, key, 'e', nonce)
    elif cipher.lower().startswith('aes'):
        return aes(data, key, 'e', aes_mode)

def decrypt(data, cipher, key, nonce=None, iv=None, aes_mode=AES.MODE_CTR):
    if cipher.lower().startswith('chacha'):
        return chacha20(data, key, 'd', nonce)
    elif cipher.lower().startswith('salsa'):
        return salsa20(data, key, 'd', nonce)
    elif cipher.lower().startswith('aes'):
        return aes(data, key, 'd', aes_mode)
