# RATFramework

Template classes for creating remote access trojan (RAT) programms. Of course, they (RATs) should be invisible by user, but if your program is proprietary and needs access to the Internet, it will work fine.